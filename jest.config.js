module.exports = {
  testURL: 'http://localhost',
  preset: 'jest-preset-angular',
  setupTestFrameworkScriptFile: '<rootDir>/src/setupJest.ts',
  transform: {
    '^.+\\.(ts|js|html)$': '<rootDir>/node_modules/jest-preset-angular/preprocessor.js',
    '.+\\.(css|styl|less|sass|scss|png|jpg|ttf|woff|woff2)$': 'jest-transform-stub'
  },
  coveragePathIgnorePatterns: ['/node_modules/', '/src/jestGlobalMocks.ts', '/src/setupJest.ts', '/src/environments'],
  coverageThreshold: {
    global: {
      branches: 80,
      functions: 80,
      lines: 80,
      statements: -10
    }
  },
  globals: {
    'ts-jest': {
      tsConfigFile: 'src/tsconfig.spec.json'
    },
    __TRANSFORM_HTML__: true,
    GOOGLE_API_KEY: 'key',
    AGGRID_KEY: 'key',
    ENV: 'testing'
  }
};
