import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-sanitas-button',
  templateUrl: './sanitas-button.template.html',
  styleUrls: ['./sanitas-button.style.scss'],
})
export class SanitasButtonComponent {
  @Input() public className = '';
}
