import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { SanitasButtonComponent } from './sanitas-button.component';

describe('SanitasButtonComponent', () => {
  let component: SanitasButtonComponent;
  let fixture: ComponentFixture<SanitasButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SanitasButtonComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SanitasButtonComponent);
    component = fixture.componentInstance;
  });

  it('should created', () => {
    expect(component).toBeTruthy();
  });
});
