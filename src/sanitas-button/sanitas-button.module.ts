import { NgModule } from '@angular/core';
import { SanitasButtonComponent } from './sanitas-button.component';

@NgModule({
  declarations: [SanitasButtonComponent],
  exports: [SanitasButtonComponent],
  entryComponents: [SanitasButtonComponent],
})
export class SanitasButtonModule {}
